package br.com.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.model.Image;

@Repository
public class ImageDao {
	@PersistenceContext
	private EntityManager manager;

	/*@Transactional*/
	public void create(Image Image) {
		manager.persist(Image);
	}
/*
	@Transactional*/
	public void update(Image Image) {
		manager.merge(Image);
	}
/*
	@Transactional*/
	public void delete(Long id) {
		Image Image = getById(id);
		manager.remove(Image);

	}

	public Image getById(Long id) {
		return manager.find(Image.class, id);
	}

	public List<Image> getAll() {
		TypedQuery<Image> query = manager.createQuery("SELECT i FROM Image i", Image.class);
		return query.getResultList();
	}

}
