package br.com.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import br.com.model.Product;

@Repository
public class ProductDao {
	@PersistenceContext
	private EntityManager manager;
/*
	@Transactional*/
	public void create(Product product) {
		manager.persist(product);
	}
/*
	@Transactional*/
	public void update(Product product) {
		manager.merge(product);
	}
/*
	@Transactional*/
	public void delete(Long id) {
		Product product = getById(id);
		manager.remove(product);

	}

	public Product getById(Long id) {
		return manager.find(Product.class, id);
	}

	public Product getByIdWithoutRelationships(Long id_product) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Product> criteriaQuery = builder.createQuery(Product.class);
		Root<Product> produtcRoot = criteriaQuery.from(Product.class);
		Path<Product> id = produtcRoot.get("id_product");
		Path<Product> name = produtcRoot.get("product_name");
		criteriaQuery.multiselect(id, name);
		criteriaQuery.where(builder.equal(produtcRoot.get("id_product"), id_product));
		return manager.createQuery(criteriaQuery).getSingleResult();
	}

	public List<Product> getAll() {
		TypedQuery<Product> query = manager.createQuery("SELECT p FROM Product p", Product.class);
		return query.getResultList();
	}

	public List<Product> getAllWithoutRelationships() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Product> criteriaQuery = builder.createQuery(Product.class);
		Root<Product> produtcRoot = criteriaQuery.from(Product.class);
		Path<Product> id = produtcRoot.get("id_product");
		Path<Product> name = produtcRoot.get("product_name");
		criteriaQuery.multiselect(id, name);
		return manager.createQuery(criteriaQuery).getResultList();
	}
}
