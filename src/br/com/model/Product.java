package br.com.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Component
@JsonIgnoreProperties("parent")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_product;

	@Column(nullable = false)
	private String product_name;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private List<Image> images;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "parent", targetEntity = Product.class, orphanRemoval = false, cascade = {
			CascadeType.REMOVE, CascadeType.MERGE })
	private List<Product> childrens;

	@ManyToOne(targetEntity = Product.class, optional = true)
	@JoinTable(name = "product_with_produtcs", joinColumns = @JoinColumn(name = "id_child"), inverseJoinColumns = @JoinColumn(name = "id_parent"))
	private Product parent;

	public Product() {
		super();
	}

	public Product(Long id_product, String product_name) {
		super();
		this.id_product = id_product;
		this.product_name = product_name;
	}

	public Long getId_product() {
		return id_product;
	}

	public void setId_product(Long id_product) {
		this.id_product = id_product;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public List<Product> getChildrens() {
		return childrens;
	}

	public void setChildrens(List<Product> childrens) {
		this.childrens = childrens;
	}

	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		return "Product [id_product=" + id_product + ", product_name=" + product_name + ", images=" + images
				+ ", childrens=" + childrens + ", parent=" + parent + "]";
	}

}
