package br.com.restcontroller;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.dao.ProductDao;
import br.com.model.Image;
import br.com.model.Product;

@RestController
public class ProductRestController {
	@Autowired
	private ProductDao productDao;

	@RequestMapping(value = "/product", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Product> create(@RequestBody Product product, HttpServletRequest req) {
		try {
			if (product.getImages() == null && product.getChildrens() == null) {
				productDao.create(product);
			} else if (product.getChildrens() == null) {
				for (Image image : product.getImages()) {
					image.setProduct(product);
				}
				productDao.create(product);
			} else if (product.getImages() == null) {
				productDao.create(product);
				for (Product child : product.getChildrens()) {
					child.setParent(product);
					if (child.getImages() != null) {
						for (Image image : child.getImages()) {
							image.setProduct(child);
						}
					}
				}
				productDao.update(product);

			} else {
				for (Image image : product.getImages()) {
					image.setProduct(product);
				}
				productDao.create(product);
				for (Product child : product.getChildrens()) {
					child.setParent(product);
					if (child.getImages() != null) {
						for (Image image : child.getImages()) {
							image.setProduct(child);
						}
					}
				}
				productDao.update(product);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Product>(HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.created(URI.create("/product/" + product.getId_product())).body(product);
	}

	@RequestMapping(value = "/product", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Product> getAll(HttpServletRequest req) {
		try {
			return productDao.getAll();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/productWithoutRelationships", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Product> getAllWithoutRelationships(HttpServletRequest req) {
		try {
			return productDao.getAllWithoutRelationships();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Product getById(@PathVariable long id) {
		return productDao.getById(id);
	}

	@RequestMapping(value = "/productWithoutRelationships/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Product getByIdWithoutRelationships(@PathVariable long id) {
		return productDao.getByIdWithoutRelationships(id);
	}

	@RequestMapping(value = "/productChildProducts/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Product> getByIdChildProducts(@PathVariable long id) {
		return productDao.getById(id).getChildrens();
	}

	@RequestMapping(value = "/productProductsImages/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Image> getByIdProductsImages(@PathVariable long id) {
		return productDao.getById(id).getImages();
	}

	@RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		productDao.delete(id);
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/product", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Product> update(@RequestBody Product product) {
		try {
			if (product.getImages() == null && product.getChildrens() == null) {
				productDao.update(product);
			} else if (product.getChildrens() == null) {
				for (Image image : product.getImages()) {
					image.setProduct(product);
				}
				productDao.update(product);
			} else if (product.getImages() == null) {
				productDao.update(product);
				for (Product child : product.getChildrens()) {
					child.setParent(product);
					if (child.getImages() != null) {
						for (Image image : child.getImages()) {
							image.setProduct(child);
						}
					}
				}
				productDao.update(product);

			} else {
				for (Image image : product.getImages()) {
					image.setProduct(product);
				}
				productDao.update(product);
				for (Product child : product.getChildrens()) {
					child.setParent(product);
					if (child.getImages() != null) {
						for (Image image : child.getImages()) {
							image.setProduct(child);
						}
					}
				}
				productDao.update(product);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Product>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}
}
