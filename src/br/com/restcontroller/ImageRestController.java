package br.com.restcontroller;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.dao.ImageDao;
import br.com.model.Image;

@RestController
public class ImageRestController {
	@Autowired
	private ImageDao imageDao;

	@RequestMapping(value = "/image", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Image> create(@RequestBody Image image, HttpServletRequest req) {
		try {
			imageDao.create(image);
			return ResponseEntity.created(URI.create("/image/" + image.getId_image())).body(image);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Image>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/image", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Image> getAll(HttpServletRequest req) {
		try {
			return imageDao.getAll();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/image/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		imageDao.delete(id);
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/image", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Image> update(@RequestBody Image image) {
		try {
			imageDao.update(image);
			return new ResponseEntity<Image>(image, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Image>(HttpStatus.BAD_REQUEST);
		}
	}
}
