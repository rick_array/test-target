# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Teste da TargetIT
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Usando a IDE do Eclipse
* Inicie o projeto com servidor Apache Tomcat
* Maven ir� fazer o download das depend�ncias
* O Banco de Dados utilizado foi o MySql Workbench. Apenas cria um novo schema com o nome "test-taget". Hibernate ir� trabalhar no resto. 
* Rode os testes utilizando o Postman com os links usando Json no corpo para 'criar' ou 'alterar' alguma entidade.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### O que foi solicitado mas n�o entregue ###

* The tests must be started with the mvn test command / Nunca usei o Maven dessa forma. Decidi por me concentrar nos primeiros requisitos;
* The application must start with a Maven command: mvn exec:java, mvn jetty:run, mvn spring-boot:run, etc / O mesmo;
* An embedded in-memory database should be used: either H2, HSQL, SQLite or Derby / N�o me atentei para este requisito, pois o mesmo estava fora de minha pretens�es;
